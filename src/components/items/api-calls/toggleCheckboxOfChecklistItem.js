import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const { VITE_TRELLO_BASE_URL, VITE_TRELLO_KEY, VITE_TRELLO_TOKEN } = import.meta.env;

export const toggleCheckboxOfChecklistItem = createAsyncThunk(
  "checklist/toggleCheckboxOfChecklistItem",
  async ({ idCard, idCheckItem, idChecklist, checkStatus }) => {
    const response = await axios.put(
      `${VITE_TRELLO_BASE_URL}/cards/${idCard}/checkItem/${idCheckItem}?key=${VITE_TRELLO_KEY}&token=${VITE_TRELLO_TOKEN}`,
      {
        state: checkStatus,
      }
    );
    return response.status === 200 ? { idChecklist, idChecklistItem: idCheckItem, checkStatus } : null;
  }
);
