import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const { VITE_TRELLO_BASE_URL, VITE_TRELLO_KEY, VITE_TRELLO_TOKEN } = import.meta.env;

export const createChecklistItem = createAsyncThunk(
  "checklist/createChecklistItem",
  async ({ id, name }) => {
    const response = await axios.post(
      `${VITE_TRELLO_BASE_URL}checklists/${id}/checkItems?name=${name}&key=${VITE_TRELLO_KEY}&token=${VITE_TRELLO_TOKEN}`
    );
    return { idChecklist: id, newItem: response.data };
  }
);
