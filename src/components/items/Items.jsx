import { useCallback } from "react";
import { useDispatch } from "react-redux";

import { ChecklistItemView, AddCheckItem } from "./item-utils";
import {
  deleteChecklistItem,
  createChecklistItem,
  toggleCheckboxOfChecklistItem,
} from "./api-calls";

export function Items(prop) {
  const { checklist } = prop;
  const dispatch = useDispatch();

  const handleDeleteChecklistItem = (idChecklist, idChecklistItem) => {
    dispatch(deleteChecklistItem({ idChecklist, idChecklistItem }));
  };

  const handleCreateChecklistItem = (idChecklist, name) => {
    dispatch(createChecklistItem({ id: idChecklist, name }));
  };

  const handleToggleCheckbox = (
    idCard,
    idCheckItem,
    idChecklist,
    checkStatus
  ) => {
    dispatch(
      toggleCheckboxOfChecklistItem({
        idCard,
        idCheckItem,
        idChecklist,
        checkStatus,
      })
    );
  };

  const itemRecordWithResToChecklist = useCallback((checklist) => {
    return checklist.checkItems.filter(
      (item) => item.idChecklist === checklist.id
    );
  }, []);

  return (
    <div>
      {itemRecordWithResToChecklist(checklist).length > 0 &&
        itemRecordWithResToChecklist(checklist).map((item) => {
          return (
            <ChecklistItemView
              key={item.id}
              item={item}
              handleDeleteChecklistItem={handleDeleteChecklistItem}
              handleToggleCheckbox={handleToggleCheckbox}
              checklist={checklist}
            />
          );
        })}
      {itemRecordWithResToChecklist(checklist).length === 0 && (
        <span>Items not found ...</span>
      )}
      <AddCheckItem
        handleCreateChecklistItem={handleCreateChecklistItem}
        checklist={checklist}
      />
    </div>
  );
}
