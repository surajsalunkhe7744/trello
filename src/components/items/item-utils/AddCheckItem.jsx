import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  Typography,
} from "@mui/material";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { useState } from "react";

const addingChecklistOption = {
  enable: "enable",
  disable: "disable",
};

export function AddCheckItem(prop) {
  const { handleCreateChecklistItem, checklist } = prop;
  const { enable, disable } = addingChecklistOption;

  const [isAddingItem, setIsAddingItem] = useState(false);
  const [checkItemName, setCheckItemName] = useState("");
  const [isValidItemName, setIsValidItemName] = useState(true);

  const handleAddItem = (name) => {
    if (name.length > 0) {
      handleCreateChecklistItem(checklist.id, name);
      handleAddingItem(disable)
    } else {
      setIsValidItemName(false);
    }
  };

  const handleAddingItem = (type) => {
    switch (type) {
      case enable: {
        setIsAddingItem(true);
        return;
      }
      case disable: {
        setIsAddingItem(false);
        setIsValidItemName(true);
        setCheckItemName("");
        return;
      }
      default:
        return;
    }
  };

  const handleSetItemName = (name) => {
    const isValidInput = name.charAt(0) === " ";
    if (!isValidInput) {
      setCheckItemName(name);
      setIsValidItemName(true);
    }
  };

  return (
    <div>
      {!isAddingItem && (
        <Typography
          sx={{
            backgroundColor: "#d9d9d9",
            width: "120px",
            height: "35px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            fontWeight: 600,
            borderRadius: "4px",
            cursor: "pointer",
            marginTop: "15px",
          }}
          onClick={() => handleAddingItem(enable)}
        >
          Add an item
        </Typography>
      )}
      {isAddingItem && (
        <Card
          sx={{
            height: "100%",
            display: "flex",
            borderRadius: "4px",
            cursor: "pointer",
            marginTop: "10px",
          }}
        >
          <CardContent>
            <TextField
              label="Enter item title ..."
              variant="outlined"
              value={checkItemName}
              onChange={(event) => handleSetItemName(event.target.value)}
              helperText={!isValidItemName && "Enter valid list name"}
              error={!isValidItemName}
            />
            <Box sx={{ display: "flex", marginTop: "10px" }}>
              <Button
                variant="contained"
                onClick={() => handleAddItem(checkItemName)}
              >
                Add Checkist
              </Button>
              <CloseRoundedIcon
                color="action"
                sx={{
                  height: "30px",
                  width: "30px",
                  marginLeft: "10px",
                }}
                onClick={() => handleAddingItem(disable)}
              />
            </Box>
          </CardContent>
        </Card>
      )}
    </div>
  );
}
