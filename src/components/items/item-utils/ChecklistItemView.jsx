import { Checkbox } from "@mui/material";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";

export function ChecklistItemView(prop) {
  const { item, handleDeleteChecklistItem, handleToggleCheckbox, checklist } =
    prop;

  return (
    <div
      key={item.id}
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        <Checkbox
          label={item.name}
          sx={{ padding: "0px" }}
          checked={item.state === "complete"}
          onClick={() =>
            handleToggleCheckbox(
              checklist.idCard,
              item.id,
              checklist.id,
              item.state === "complete" ? "incomplete" : "complete"
            )
          }
        />
        <p
          style={{
            marginLeft: "10px",
            textDecoration: item.state === "complete" ? "line-through" : "none",
          }}
        >
          {item.name}
        </p>
      </div>
      <RemoveCircleOutlineIcon
        sx={{ cursor: "pointer" }}
        onClick={() => handleDeleteChecklistItem(checklist.id, item.id)}
      />
    </div>
  );
}
