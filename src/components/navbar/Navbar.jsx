import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";

export function Navbar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Link to="/">
            <img
              src="https://avatars.slack-edge.com/2021-07-19/2282472048054_9a51d280179d828b3ad7_512.png"
              alt="logo"
              style={{
                height: "35px",
                width: "35px",
                cursor: "pointer"
              }}
            />
          </Link>
          <Typography
            variant="h4"
            component="div"
            sx={{
              flexGrow: 1,
              marginTop: "10px",
              display: "flex",
              justifyContent: "center",
            }}
          >
            Trello
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
