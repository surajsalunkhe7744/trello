import { Box, Button, CardContent, TextField, Typography } from "@mui/material";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import AddRoundedIcon from "@mui/icons-material/AddRounded";
import { useState } from "react";

const addingCard = {
  enable: "enable",
  disable: "disable",
};

export function AddCard(prop) {
  const { list, handleCreateCard } = prop;
  const { enable, disable } = addingCard;

  const [isAddingCard, setIsAddingCard] = useState(false);
  const [cardName, setCardName] = useState("");
  const [isValidCardName, setIsValidCardName] = useState(true);

  const handleAddingCard = (type) => {
    switch (type) {
      case enable: {
        setIsAddingCard(true);
        return;
      }
      case disable: {
        setIsAddingCard(false);
        setIsValidCardName(true);
        setCardName("");
        return;
      }
      default:
        return;
    }
  };

  const handleSetListName = (name) => {
    const isValidInput = name.charAt(0) === " ";
    if (!isValidInput) {
      setCardName(name);
      setIsValidCardName(true);
    }
  };

  const handleAddCard = (id, name) => {
    if (name.length > 0) {
      handleCreateCard(id, name);
      setCardName("");
      setIsAddingCard(false);
    } else {
      setIsValidCardName(false);
    }
  };

  return (
    <CardContent>
      {!isAddingCard && (
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
            paddingBottom: "0px",
          }}
          onClick={() => handleAddingCard(enable)}
        >
          <AddRoundedIcon color="action" />
          <Typography color="text.secondary">Add a card</Typography>
        </Box>
      )}
      {isAddingCard && (
        <>
          <TextField
            label="Enter card name ..."
            variant="outlined"
            value={cardName}
            onChange={(event) => handleSetListName(event.target.value)}
            helperText={!isValidCardName && "Enter valid list name"}
            error={!isValidCardName}
          />
          <Box sx={{ display: "flex", marginTop: "10px" }}>
            <Button
              variant="contained"
              onClick={() => handleAddCard(list.id, cardName)}
            >
              Add Card
            </Button>
            <CloseRoundedIcon
              color="action"
              sx={{
                height: "30px",
                width: "30px",
                marginLeft: "10px",
                cursor: "pointer",
              }}
              onClick={() => handleAddingCard(disable)}
            />
          </Box>
        </>
      )}
    </CardContent>
  );
}
