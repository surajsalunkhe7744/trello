import HighlightOffOutlinedIcon from "@mui/icons-material/HighlightOffOutlined";
import SubtitlesOutlinedIcon from "@mui/icons-material/SubtitlesOutlined";
import { Box, Dialog, DialogContent, DialogTitle } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useState } from "react";
import { Checklist } from "../../checklists/Checklist";

export function CardView(prop) {
  const { card, handleDeleteCard } = prop;
  const [openCard, setOpenCard] = useState(false);

  const handleSetClickOpen = () => {
    setOpenCard(!openCard);
  };

  return (
    <>
      <p
        style={{
          backgroundColor: "#fff",
          borderRadius: "4px",
          padding: "6px 6px 6px 12px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          cursor: "pointer",
        }}
        onClick={handleSetClickOpen}
      >
        {card.name}
        <HighlightOffOutlinedIcon
          sx={{
            height: "20px",
            width: "20px",
            cursor: "pointer",
            backgroundColor: "#f1f1f1",
            padding: "6px",
            borderRadius: "4px",
          }}
          onClick={(event) => {
            event.preventDefault();
            handleDeleteCard(card.id);
          }}
        />
      </p>
      <Dialog open={openCard} onClose={handleSetClickOpen}>
        <Box sx={{ width: "400px", backgroundColor: "#f1f1f1" }}>
          <DialogTitle
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <SubtitlesOutlinedIcon sx={{marginRight: "10px"}}/>
              {card.name}
            </div>
            <CloseIcon
              onClick={handleSetClickOpen}
              sx={{ cursor: "pointer" }}
            />
          </DialogTitle>
          <DialogContent>
            <Checklist card={card} />
          </DialogContent>
        </Box>
      </Dialog>
    </>
  );
}
