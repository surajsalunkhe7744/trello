import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Box } from "@mui/material";

import { AddCard, CardView } from "./card-utils";
import { getCards, createCard, deleteCard } from "./api-calls";

export function Cards(prop) {
  const { list } = prop;
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.cards.loading);
  const cardsRecord = useSelector((state) => state.cards.cardsRecord);
  const error = useSelector((state) => state.cards.error);

  useEffect(() => {
    handleGetCards();
  }, []);

  const handleGetCards = () => {
    dispatch(getCards({ id: list.idBoard }));
  };

  const handleCreateCard = (id, name) => {
    dispatch(createCard({ id, name }));
  };

  const handleDeleteCard = (id) => {
    dispatch(deleteCard({ id }));
  };

  return (
    <Box>
      {loading && <span>Loading ...</span>}
      {!loading && error && <span>Error ...</span>}
      {!loading && !error && (
        <>
          {cardsRecord
            .filter((card) => card.idList === list.id)
            .map((card) => {
              return <CardView key={card.id} card={card} handleDeleteCard={handleDeleteCard} />;
            })}
        </>
      )}
      {!loading && !error && cardsRecord.length === 0 && (
        <span>Cards Not Found ...</span>
      )}
      <AddCard handleCreateCard={handleCreateCard} list={list} />
    </Box>
  );
}
