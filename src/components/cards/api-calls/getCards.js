import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const { VITE_TRELLO_BASE_URL, VITE_TRELLO_KEY, VITE_TRELLO_TOKEN } = import.meta.env;

export const getCards = createAsyncThunk("cards/getCards", async ({ id }) => {
  const response = await axios.get(
    `${VITE_TRELLO_BASE_URL}boards/${id}/cards?key=${VITE_TRELLO_KEY}&token=${VITE_TRELLO_TOKEN}`
  );
  return response.data;
});
