import { useCallback } from "react";
import LibraryAddCheckOutlinedIcon from "@mui/icons-material/LibraryAddCheckOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";

import { Items } from "../../items/Items";

export function ChecklistView(prop) {
  const { card, checklistsRecord, handleDeleteChecklist } = prop;

  const checklistRecordWithResToCard = useCallback(() => {
    return checklistsRecord.filter((checklist) => checklist.idCard === card.id);
  }, [checklistsRecord, card.id]);

  return (
    <div>
      {checklistRecordWithResToCard().length === 0 && (
        <span>Checklist not found ...</span>
      )}
      {checklistRecordWithResToCard().length > 0 &&
        checklistRecordWithResToCard().map((checklist) => {
          return (
            <div
              key={checklist.id}
              style={{
                backgroundColor: "#fff",
                borderRadius: "4px",
                marginTop: "15px",
                padding: "5px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    alignItems: "center",
                  }}
                >
                  <LibraryAddCheckOutlinedIcon sx={{ marginRight: "10px" }} />
                  <p>{checklist.name}</p>
                </div>
                <DeleteOutlinedIcon
                  sx={{ cursor: "pointer" }}
                  onClick={() => handleDeleteChecklist(checklist.id)}
                />
              </div>
              <Items checklist={checklist} />
            </div>
          );
        })}
    </div>
  );
}
