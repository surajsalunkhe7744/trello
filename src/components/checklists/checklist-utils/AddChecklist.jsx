import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  Typography,
} from "@mui/material";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { useState } from "react";

const addingChecklistOption = {
  enable: "enable",
  disable: "disable",
};

export function AddChecklist(prop) {
  const { handleCreateChecklist } = prop;
  const { enable, disable } = addingChecklistOption;

  const [isAddingChecklist, setIsAddingChecklist] = useState(false);
  const [checklistName, setChecklistName] = useState("");
  const [isValidChecklistName, setIsValidChecklistName] = useState(true);

  const handleAddChecklist = (name) => {
    if (name.length > 0) {
      handleCreateChecklist(name);
      setChecklistName("");
    } else {
      setIsValidChecklistName(false);
    }
  };

  const handleAddingChecklist = (type) => {
    switch (type) {
      case enable: {
        setIsAddingChecklist(true);
        return;
      }
      case disable: {
        setIsAddingChecklist(false);
        setIsValidChecklistName(true);
        setChecklistName("");
        return;
      }
      default:
        return;
    }
  };

  const handleSetChecklistName = (name) => {
    const isValidInput = name.charAt(0) === " ";
    if (!isValidInput) {
      setChecklistName(name);
      setIsValidChecklistName(true);
    }
  };
  
  return (
    <div>
      {!isAddingChecklist && (
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "center",
            marginTop: "30px",
            cursor: "pointer",
          }}
          onClick={() => handleAddingChecklist(enable)}
        >
          <AddOutlinedIcon />
          <Typography>Add a Checklist</Typography>
        </div>
      )}
      {isAddingChecklist && (
        <Card
          sx={{
            height: "100%",
            display: "flex",
            borderRadius: "4px",
            cursor: "pointer",
            marginTop: "25px",
          }}
        >
          <CardContent>
            <TextField
              label="Enter checklist title ..."
              variant="outlined"
              value={checklistName}
              onChange={(event) => handleSetChecklistName(event.target.value)}
              helperText={!isValidChecklistName && "Enter valid list name"}
              error={!isValidChecklistName}
            />
            <Box sx={{ display: "flex", marginTop: "10px" }}>
              <Button
                variant="contained"
                onClick={() => handleAddChecklist(checklistName)}
              >
                Add Checkist
              </Button>
              <CloseRoundedIcon
                color="action"
                sx={{ height: "30px", width: "30px", marginLeft: "10px" }}
                onClick={() => handleAddingChecklist(disable)}
              />
            </Box>
          </CardContent>
        </Card>
      )}
    </div>
  );
}
