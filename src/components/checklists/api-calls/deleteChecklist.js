import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const { VITE_TRELLO_BASE_URL, VITE_TRELLO_KEY, VITE_TRELLO_TOKEN } = import.meta.env;

export const deleteChecklist = createAsyncThunk(
  "checklist/deleteChecklist",
  async ({ id }) => {
    const response = await axios.delete(
      `${VITE_TRELLO_BASE_URL}checklists/${id}?key=${VITE_TRELLO_KEY}&token=${VITE_TRELLO_TOKEN}`
    );
    return response.status === 200 ? id : null;
  }
);
