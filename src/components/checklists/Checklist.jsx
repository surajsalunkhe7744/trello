import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getChecklists, deleteChecklist, createChecklist } from "./api-calls";
import { AddChecklist, ChecklistView } from "./checklist-utils";

export function Checklist(prop) {
  const { card } = prop;
  const dispatch = useDispatch();

  const checklistsRecord = useSelector(
    (state) => state.checklists.checklistsRecord
  );
  const loading = useSelector((state) => state.checklists.loading);
  const error = useSelector((state) => state.checklists.error);

  useEffect(() => {
    handleGetChecklists();
  }, []);

  const handleCreateChecklist = (name) => {
    dispatch(createChecklist({ id: card.id, name }));
  };

  const handleGetChecklists = () => {
    dispatch(getChecklists({ id: card.idBoard }));
  };

  const handleDeleteChecklist = (id) => {
    dispatch(deleteChecklist({ id }));
  };

  return (
    <div>
      {loading && <span>Loading ...</span>}
      {error && !loading && <span>Error ...</span>}
      {!loading && !error && (
        <>
          <ChecklistView
            handleDeleteChecklist={handleDeleteChecklist}
            card={card}
            checklistsRecord={checklistsRecord}
          />
          <AddChecklist handleCreateChecklist={handleCreateChecklist} />
        </>
      )}
    </div>
  );
}
