import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Box } from "@mui/material";

import { getBoards, deleteBoard, createBoard } from "./api-calls";
import { Loader, Error } from "../../utils";
import { AddBoard, BoardView } from "./board-utils";

export function Boards() {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.boards.loading);
  const boards = useSelector((state) => state.boards.boardsRecord);
  const error = useSelector((state) => state.boards.error);

  useEffect(() => {
    handleGetBoards();
  }, []);

  const handleGetBoards = () => {
    dispatch(getBoards());
  };

  const handleDeleteBoard = (id) => {
    dispatch(deleteBoard(id));
  };

  const handleCreateBoard = (name) => {
    dispatch(createBoard(name));
  };

  return (
    <Box>
      {loading && <Loader />}
      {!loading && error && (
        <Error
          error={error}
          backBotton="back to boards"
          handleClick={handleGetBoards}
        />
      )}
      {!loading && !error && (
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              gap: "40px",
              margin: "80px 0px 0px 40px",
              width: "85%",
            }}
          >
            {boards.map((board) => {
              return (
                <Link
                  to={`/${board.id}`}
                  key={board.id}
                  style={{ textDecoration: "none" }}
                >
                  <BoardView
                    board={board}
                    handleDeleteBoard={handleDeleteBoard}
                  />
                </Link>
              );
            })}
            {boards.length < 10 && (
              <AddBoard handleCreateBoard={handleCreateBoard} />
            )}
          </Box>
        </Box>
      )}
    </Box>
  );
}
