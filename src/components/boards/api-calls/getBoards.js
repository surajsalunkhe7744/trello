import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const { VITE_TRELLO_BASE_URL, VITE_TRELLO_KEY, VITE_TRELLO_TOKEN } = import.meta.env;

export const getBoards = createAsyncThunk("boards/getBoards",async () => {
  const response = await axios.get(
    `${VITE_TRELLO_BASE_URL}members/me/boards?key=${VITE_TRELLO_KEY}&token=${VITE_TRELLO_TOKEN}`
  );
  return response.data;
});
