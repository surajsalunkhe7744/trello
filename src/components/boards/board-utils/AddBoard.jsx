import {
  Button,
  Card,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from "@mui/material";
import AddRoundedIcon from "@mui/icons-material/AddRounded";
import { useState } from "react";

export function AddBoard(prop) {
  const { handleCreateBoard } = prop;
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [boardName, setBoardName] = useState("");
  const [isValidBoardName, setIsValidBoardName] = useState(true);

  const handleAddBoard = (name) => {
    if (name.length > 0) {
      handleCreateBoard(name);
      handleCloseModal();
    } else {
      setIsValidBoardName(false);
    }
  };

  const handleOpenModal = () => {
    setIsOpenModal(true);
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
    setIsValidBoardName(true);
    setBoardName("");
  };

  const handleSetBoardName = (name) => {
    const isValidInput = name.charAt(0) === " ";
    if (!isValidInput) {
      setBoardName(name);
      setIsValidBoardName(true);
    }
  };

  return (
    <>
      <Card
        sx={{
          width: 350,
          height: 205,
          background: "#f1f1f1",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          cursor: "pointer",
        }}
        onClick={handleOpenModal}
      >
        <AddRoundedIcon
          variant="primary"
          sx={{ height: "80px", width: "80px" }}
        />
        <Typography>Add Board</Typography>
      </Card>
      <Dialog open={isOpenModal} onClose={handleCloseModal}>
        <DialogTitle>Add Board</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To create a new board in your workspace, please enter your board
            name here. After that click on the create board button.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Board Name"
            type="text"
            fullWidth
            variant="standard"
            value={boardName}
            onChange={(event) => handleSetBoardName(event.target.value)}
            error={!isValidBoardName}
            helperText={!isValidBoardName && "Please enter board name"}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseModal}>Cancel</Button>
          <Button onClick={() => handleAddBoard(boardName)}>
            Create Board
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
