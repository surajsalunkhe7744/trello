import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Tooltip,
  Typography,
} from "@mui/material";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";

export function BoardView(prop) {
  const { board, handleDeleteBoard } = prop;
  const { backgroundImage, backgroundColor } = board.prefs;
  
  return (
    <Card sx={{ width: 350, position: "relative" }}>
      <CardActionArea>
        {backgroundImage && (
          <CardMedia
            component="img"
            height="140"
            image={backgroundImage}
            alt={`board${board.id}`}
          />
        )}
        {backgroundColor && (
          <Box
            sx={{
              backgroundColor: backgroundColor,
              height: "140px",
            }}
          ></Box>
        )}
        <CardContent>
          <Tooltip
            title={<Typography variant="h6">{board.name}</Typography>}
            placement="top-start"
          >
            <Typography
              variant="h6"
              sx={{
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
              }}
            >
              {board.name}
            </Typography>
          </Tooltip>
        </CardContent>
      </CardActionArea>

      <DeleteOutlinedIcon
        sx={{
          cursor: "pointer",
          position: "absolute",
          top: 10,
          right: 10,
          backgroundColor: "#ffffff",
          borderRadius: "4px",
        }}
        onClick={(event) => {
          event.preventDefault();
          handleDeleteBoard(board.id);
        }}
      />
    </Card>
  );
}
