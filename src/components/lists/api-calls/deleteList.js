import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const { VITE_TRELLO_BASE_URL, VITE_TRELLO_KEY, VITE_TRELLO_TOKEN } = import.meta.env;

export const deleteList = createAsyncThunk("lists/deleteList",async (id) => {
  const response = await axios.put(
    `${VITE_TRELLO_BASE_URL}lists/${id}/closed?key=${VITE_TRELLO_KEY}&token=${VITE_TRELLO_TOKEN}`,
    {value : true}
  );
  return response.status === 200 ? id : null;
});