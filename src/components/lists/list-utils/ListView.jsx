import { Box, CardContent, Tooltip, Typography } from "@mui/material";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";

export function ListView(prop) {
  const { list, handleDeleteList } = prop;

  return (
    <CardContent>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Tooltip
          title={<Typography>{list.name}</Typography>}
          placement="top-start"
        >
          <Typography
            sx={{
              whiteSpace: "nowrap",
              overflow: "hidden",
              textOverflow: "ellipsis",
            }}
          >
            {list.name}
          </Typography>
        </Tooltip>
        <DeleteOutlinedIcon
          sx={{
            cursor: "pointer",
            backgroundColor: "#fff",
            padding: "4px",
            borderRadius: "4px",
            marginRight: "4px",
          }}
          onClick={() => handleDeleteList(list.id)}
        />
      </Box>
    </CardContent>
  );
}
