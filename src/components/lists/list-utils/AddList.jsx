import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  Typography,
} from "@mui/material";
import AddRoundedIcon from "@mui/icons-material/AddRounded";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { useState } from "react";

const addingListOption = {
  enable: "enable",
  disable: "disable",
};

export function AddList(prop) {
  const { handleCreateList } = prop;
  const { enable, disable } = addingListOption;

  const [isAddingList, setIsAddingList] = useState(false);
  const [listName, setListName] = useState("");
  const [isValidListName, setIsValidListName] = useState(true);

  const handleAddList = (name) => {
    if (name.length > 0) {
      handleCreateList(name);
    } else {
      setIsValidListName(false);
    }
  };

  const handleAddingList = (type) => {
    switch (type) {
      case enable: {
        setIsAddingList(true);
        return;
      }
      case disable: {
        setIsAddingList(false);
        setIsValidListName(true);
        setListName("");
        return;
      }
      default:
        return;
    }
  };

  const handleSetListName = (name) => {
    const isValidInput = name.charAt(0) === " ";
    if (!isValidInput) {
      setListName(name);
      setIsValidListName(true);
    }
  };

  return (
    <>
      {!isAddingList && (
        <Card
          sx={{
            width: "275px",
            height: "100%",
            backgroundColor: "#f1f1f1",
            marginLeft: "20px",
            display: "flex",
            padding: "10px 0 0 0",
            borderRadius: "4px",
            cursor: "pointer",
          }}
          onClick={() => handleAddingList(enable)}
        >
          <CardContent sx={{ display: "flex" }}>
            <AddRoundedIcon color="action" />
            <Typography sx={{ fontSize: "18px" }} color="text.secondary">
              Add another list
            </Typography>
          </CardContent>
        </Card>
      )}
      {isAddingList && (
        <Card
          sx={{
            width: "275px",
            height: "100%",
            backgroundColor: "#f1f1f1",
            marginLeft: "20px",
            display: "flex",
            padding: "10px 0 0 0",
            borderRadius: "4px",
            cursor: "pointer",
          }}
        >
          <CardContent>
            <TextField
              label="Enter list title ..."
              variant="outlined"
              value={listName}
              onChange={(event) => handleSetListName(event.target.value)}
              helperText={!isValidListName && "Enter valid list name"}
              error={!isValidListName}
            />
            <Box sx={{ display: "flex", marginTop: "10px" }}>
              <Button
                variant="contained"
                onClick={() => handleAddList(listName)}
              >
                Add List
              </Button>
              <CloseRoundedIcon
                color="action"
                sx={{ height: "30px", width: "30px", marginLeft: "10px" }}
                onClick={() => handleAddingList(disable)}
              />
            </Box>
          </CardContent>
        </Card>
      )}
    </>
  );
}
