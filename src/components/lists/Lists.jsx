import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Box, Card, CardContent } from "@mui/material";

import { getLists, deleteList, createList } from "./api-calls";
import { Loader, Error } from "../../utils";
import { Cards } from "../cards/Cards";
import { AddList, ListView } from "./list-utils";

export function List() {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.lists.loading);
  const lists = useSelector((state) => state.lists.listsRecord);
  const error = useSelector((state) => state.lists.error);

  const { id } = useParams();

  useEffect(() => {
    handleGetLists();
  }, []);

  const handleGetLists = () => {
    dispatch(getLists({ id }));
  };

  const handleDeleteList = (id) => {
    dispatch(deleteList(id));
  };

  const handleCreateList = (name) => {
    dispatch(createList({ id, name }));
  };

  return (
    <div>
      {loading && <Loader />}
      {!loading && error && (
        <Error
          backBotton="back to lists"
          error={error}
          handleClick={handleGetLists}
        />
      )}
      {!loading && !error && (
        <Box sx={{ display: "flex", borderRadius: "10px", padding: "20px" }}>
          {lists.map((list) => {
            return (
              <Card
                key={list.id}
                sx={{
                  width: "275px",
                  height: "100%",
                  backgroundColor: "#f1f1f1",
                  marginLeft: "20px",
                  padding: "12px 8px 8px 8px",
                }}
              >
                <ListView list={list} handleDeleteList={handleDeleteList} />
                <CardContent>
                  <Cards list={list} />
                </CardContent>
              </Card>
            );
          })}
          <AddList handleCreateList={handleCreateList} />
        </Box>
      )}
    </div>
  );
}
