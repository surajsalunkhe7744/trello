import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import { Boards } from "./components/boards/Boards";
import { Navbar } from "./components/navbar/Navbar";
import { List } from "./components/lists/Lists";

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route exact path="/" element={<Boards />} />
        <Route path="/:id" element={<List />} />
      </Routes>
    </Router>
  );
}

export default App;
