import { createSlice } from "@reduxjs/toolkit";

import { getLists } from "../../components/lists/api-calls/getLists";
import { createList } from "../../components/lists/api-calls/createList";
import { deleteList } from "../../components/lists/api-calls/deleteList";

const initialState = {
    listsRecord: [],
    loading: false,
    error: ""
}

const listSlice = createSlice({
    name: "lists",
    initialState,
    extraReducers: (builder) => {
        
        //reducers for the createList method.
        builder.addCase(createList.pending, (state) => {
            state.loading = true;
        })
        builder.addCase(createList.fulfilled, (state, action) => {
            state.loading = false;
            state.error = "";
            state.listsRecord = [...state.listsRecord, action.payload];
        })
        builder.addCase(createList.rejected, (state) => {
            state.loading = false;
            state.error = "Error while creating list";
        })

        //reducers for the getLists method.
        builder.addCase(getLists.pending, (state) => {
            state.loading = true;
        })
        builder.addCase(getLists.fulfilled, (state, action) => {
            state.loading = false;
            state.error = "";
            state.listsRecord = action.payload;
        })
        builder.addCase(getLists.rejected, (state) => {
            state.loading = false;
            state.error = "Error while getting lists";
        })


        //reducers for the deleteList method.
        builder.addCase(deleteList.pending, (state) => {
            state.loading = true;
        })
        builder.addCase(deleteList.fulfilled, (state, action) => {
            state.loading = false;
            state.error = "";
            state.listsRecord = state.listsRecord.filter((list) => list.id !== action.payload);
        })
        builder.addCase(deleteList.rejected, (state) => {
            state.loading = false;
            state.error = "Error while deleting list";
        })
    }
});

export default listSlice.reducer;