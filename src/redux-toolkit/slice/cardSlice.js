import { createSlice } from "@reduxjs/toolkit";

import { createCard } from "../../components/cards/api-calls/createCard";
import { deleteCard } from "../../components/cards/api-calls/deleteCard";
import { getCards } from "../../components/cards/api-calls/getCards";

const initialState = {
  cardsRecord: [],
  loading: false,
  error: "",
};

const cardSlice = createSlice({
  name: "cards",
  initialState,
  extraReducers: (builder) => {

    //reducers for the createCard method.
    builder.addCase(createCard.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createCard.fulfilled, (state, action) => {
      state.loading = false;
      state.error = "";
      state.cardsRecord = [...state.cardsRecord, action.payload];
    });
    builder.addCase(createCard.rejected, (state) => {
      state.loading = false;
      state.error = "Error while creating card";
    });

    //reducers for the getCards method.
    builder.addCase(getCards.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getCards.fulfilled, (state, action) => {
      state.loading = false;
      state.cardsRecord = action.payload;
      state.error = "";
    });
    builder.addCase(getCards.rejected, (state) => {
      state.loading = false;
      state.error = "Error while geting cards";
    });


    //reducers for the deleteCard method.
    builder.addCase(deleteCard.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteCard.fulfilled, (state, action) => {
      state.loading = false;
      state.cardsRecord = state.cardsRecord.filter(
        (card) => card.id !== action.payload
      );
    });
    builder.addCase(deleteCard.rejected, (state) => {
      state.loading = false;
      state.error = "Error while deleting card";
    });

  },
});

export default cardSlice.reducer;
