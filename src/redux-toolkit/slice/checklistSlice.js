import { createSlice } from "@reduxjs/toolkit";

import { getChecklists } from "../../components/checklists/api-calls/getChecklists";
import { deleteChecklist } from "../../components/checklists/api-calls/deleteChecklist";
import { createChecklist } from "../../components/checklists/api-calls/createChecklist";
import { deleteChecklistItem } from "../../components/items/api-calls/deleteChecklistItem";
import { createChecklistItem } from "../../components/items/api-calls/createChecklistItem";
import { toggleCheckboxOfChecklistItem } from "../../components/items/api-calls/toggleCheckboxOfChecklistItem";

const initialState = {
  loading: false,
  checklistsRecord: [],
  error: "",
};

const checklistSlice = createSlice({
  name: "checkList",
  initialState,
  extraReducers: (builder) => {
    // reducers for createChecklist method
    builder.addCase(createChecklist.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createChecklist.fulfilled, (state, action) => {
      state.loading = false;
      state.checklistsRecord = [...state.checklistsRecord, action.payload];
    });
    builder.addCase(createChecklist.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });

    // reducers for getChecklists method
    builder.addCase(getChecklists.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getChecklists.fulfilled, (state, action) => {
      state.loading = false;
      state.checklistsRecord = action.payload;
    });
    builder.addCase(getChecklists.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });

    // reducers for deleteChecklist method
    builder.addCase(deleteChecklist.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteChecklist.fulfilled, (state, action) => {
      state.loading = false;
      state.checklistsRecord = state.checklistsRecord.filter(
        (checklist) => checklist.id !== action.payload
      );
    });
    builder.addCase(deleteChecklist.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });

    // reducers for createChecklistItem method
    builder.addCase(createChecklistItem.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createChecklistItem.fulfilled, (state, action) => {
      const { idChecklist, newItem } = action.payload;
      const updatedState = { ...state };

      updatedState.checklistsRecord = state.checklistsRecord.map((checklist) =>
        checklist.id === idChecklist
          ? {
              ...checklist,
              checkItems: [...checklist.checkItems, newItem],
            }
          : checklist
      );

      state.loading = false;
      state.checklistsRecord = updatedState.checklistsRecord;
      state.error = false;
    });

    // reducers for deleteChecklistItem method
    builder.addCase(deleteChecklistItem.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteChecklistItem.fulfilled, (state, action) => {
      const { idChecklist, idChecklistItem } = action.payload;
      const updatedState = { ...state };

      updatedState.checklistsRecord = state.checklistsRecord.map((checklist) =>
        checklist.id === idChecklist
          ? {
              ...checklist,
              checkItems: checklist.checkItems.filter(
                (item) => item.id !== idChecklistItem
              ),
            }
          : checklist
      );

      state.loading = false;
      state.checklistsRecord = updatedState.checklistsRecord;
      state.error = false;
    });
    builder.addCase(deleteChecklistItem.rejected, (state) => {
      state.loading = false;
      state.error = true;
    });

    // reducers for deleteChecklistItem method
    builder.addCase(toggleCheckboxOfChecklistItem.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(
      toggleCheckboxOfChecklistItem.fulfilled,
      (state, action) => {
        const { idChecklist, idChecklistItem, checkStatus } = action.payload;
        const updatedState = { ...state };

        updatedState.checklistsRecord = state.checklistsRecord.map(
          (checklist) =>
            checklist.id === idChecklist
              ? {
                  ...checklist,
                  checkItems: checklist.checkItems.map((item) =>
                    item.id === idChecklistItem
                      ? { ...item, state: checkStatus }
                      : item
                  ),
                }
              : checklist
        );

        state.loading = false;
        state.checklistsRecord = updatedState.checklistsRecord;
        state.error = false;
      }
    );
    builder.addCase(toggleCheckboxOfChecklistItem.rejected, (state) => {
      state.loading = false;
      state.error = true;
    });
  },
});

export default checklistSlice.reducer;
