import { createSlice } from "@reduxjs/toolkit";
import { getBoards } from "../../components/boards/api-calls/getBoards";
import { createBoard } from "../../components/boards/api-calls/createBoard";
import { deleteBoard } from "../../components/boards/api-calls/deleteBoard";

const initialState = {
    boardsRecord: [],
    loading: false,
    error: ""
}

const boardSlice = createSlice({
    name: "boards",
    initialState,
    extraReducers: (builder) => {

        //reducers for the createBoard method.
        builder.addCase(createBoard.pending, (state) => {
            state.loading = true;
        })
        builder.addCase(createBoard.fulfilled, (state, action) => {
            state.loading = false;
            state.error = "";
            state.boardsRecord = [...state.boardsRecord, action.payload];
        })
        builder.addCase(createBoard.rejected, (state) => {
            state.loading = false;
            state.error = "Error while creating board";
        })
        
        //reducers for the getBoard method.
        builder.addCase(getBoards.pending, (state) => {
            state.loading = true;
        })
        builder.addCase(getBoards.fulfilled, (state, action) => {
            state.loading = false;
            state.error = "";
            state.boardsRecord = action.payload;
        })
        builder.addCase(getBoards.rejected, (state) => {
            state.loading = false;
            state.error = "Error while getting boards";
        })


        //reducers for the deleteBoard method.
        builder.addCase(deleteBoard.pending, (state) => {
            state.loading = true;
        })
        builder.addCase(deleteBoard.fulfilled, (state, action) => {
            state.loading = false;
            state.error = "";
            state.boardsRecord = state.boardsRecord.filter((board) => board.id !== action.payload);
        })
        builder.addCase(deleteBoard.rejected, (state) => {
            state.loading = false;
            state.error = "Error while deleting board";
        })
    }
});

export default boardSlice.reducer;