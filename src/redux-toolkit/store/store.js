import { configureStore } from "@reduxjs/toolkit";

import boardSlice from "../slice/boardSlice";
import listSlice from "../slice/listSlice";
import cardSlice from "../slice/cardSlice";
import checklistSlice from "../slice/checklistSlice";

const store = configureStore({
    reducer: {
        boards: boardSlice,
        lists: listSlice,
        cards: cardSlice,
        checklists: checklistSlice
    }
});

export default store;