import { Box, Button, Typography } from "@mui/material";

export function Error(prop) {
  const { error, backBotton, handleClick } = prop;
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Typography
        sx={{
          fontSize: "55px",
          display: "flex",
          justifyContent: "center",
          color: "blue",
          margin: "250px 0 40px 0",
        }}
      >
        {error} ...
      </Typography>
      <Button variant="contained" onClick={handleClick}>
        {backBotton}
      </Button>
    </Box>
  );
}
